# Private Chat Room #

Messaging system able to register users who could then chat with each other via a chat server.

* Users are able to privately message one another and able to enter separate private chat rooms to chat outside of the public channel
* Special attention paid to multi-threading paradigms, network programming

## Instructions ##
* Make sure server is running before attempting to connect client
* Right click on user via list to private message/invite to private chat

## TODO: ##
* Implement lists in private windows
* Implement menu to invite users to specific private chats that the inviter is a part of
* Make everything prettier

## Questions/Comments? ##
gary.perkins1164@gmail.com