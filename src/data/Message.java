package data;

import java.io.Serializable;
import java.util.ArrayList;

public class Message implements Serializable {
	private int type;
	private String message;
	private String username;
	private ArrayList<String> users;
	private ArrayList<String> pcUsers;
	private int[] pcTargets;
	private int roomNum;
	
	/**
	 * TYPE:
	 * 0 ---> messages sent between server/client that are not meant to be displayed
	 * 1 ---> regular message to be displayed on all client guis
	 * 2 ---> informative (login/logoff) message broadcast by the server to all client guis
	 * 3 ---> messages related to the VIP Lounge
	 * 4 ---> messages broadcast from server to users within VIP lounge that another user has left the lounge
	 * 5 ---> private messages directed at users selected from the JList of origin client
	 */
	
	public Message(int type, String message) {
		this.type = type;
		this.message = message;
	}
	public Message(int type, String message, String username) {
		this.type = type;
		this.message = message;
		this.username = username;
	}
	
	public Message(int type, String message, int roomNum) {
		this.type = type;
		this.message = message;
		this.roomNum = roomNum;
	}
	
	public Message(int type, String message, ArrayList<String> users) {
		this.type = type;
		this.message = message;
		this.users = users;
	}
	
	public Message(int type, String message, String username, ArrayList users) {
		this.type = type;
		this.message = message;
		this.username = username;
		this.users = users;
	}
	
	public Message(int type, String message, ArrayList<String> pcUsers, ArrayList<String> users) {
		this.type = type;
		this.message = message;
		this.pcUsers = pcUsers;
		this.users = users;
	}
	
	public Message(int type, String message, String username, ArrayList<String> pcUsers, int roomNum) {
		this.type = type;
		this.message = message;
		this.username = username;
		this.pcUsers = pcUsers;
		this.roomNum = roomNum;
	}
	
	public Message(int type, String message, String username, int[] pcTargets) {
		this.type = type;
		this.message = message;
		this.username = username;
		this.pcTargets = pcTargets;
	}
	
	public Message(int type, String message, String username, int[] pcTargets, int roomNum) {
		this.type = type;
		this.message = message;
		this.username = username;
		this.pcTargets = pcTargets;
		this.roomNum = roomNum;
	}
	
	
	public Message(int type, String message, String username, int roomNum) {
		this.type = type;
		this.message = message;
		this.username = username;
		this.roomNum = roomNum;
	}
	
	public int getType() {
		return type;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getUsername() {
		return username;
	}
	
	public ArrayList<String> getUsers() {
		return users;
	}
	
	public ArrayList<String> getPCUsers() {
		return pcUsers;
	}
	
	public int[] getPCTargets() {
		return pcTargets;
	}
	
	public int getRoomNum() {
		return roomNum;
	}
	
}
