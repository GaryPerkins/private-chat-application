package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import data.Message;

public class ChatClient {
	private static final int PORT = 9000;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	JFrame frame = new JFrame("Chat Client");
	JList<String> userList = new JList<String>();
	JList<String> pcUserList = new JList<String>();
	JTextField textField = new JTextField(40);
	JTextArea messageArea = new JTextArea(8, 40);
	JTextArea pcMessageArea = new JTextArea(8, 40);
	JButton sendBtn = new JButton("Send");
	JButton logOffBtn = new JButton("Log Off");
	JPopupMenu popup;
	JScrollPane userListScroll;
	JScrollPane messageAreaScroll;
	DefaultListModel listModel;
	private String username;
	private boolean inLounge = false;
	private JLabel pmLabel = new JLabel("Private message: ");
	private JLabel nameLabel;
	private int[] pmTargets;
	private JPanel namePanel;
	
	//new
	ArrayList<PrivateChat> pChatRooms = new ArrayList<PrivateChat>();
	ArrayList<Integer> pChatNums = new ArrayList<Integer>();
	
	public ChatClient() {
		textField.setEditable(false);
		messageArea.setEditable(false);
		pcMessageArea.setEditable(false);
		
		popup = new JPopupMenu();
		JMenuItem pmItem = new JMenuItem("Private Message");
		JMenuItem startPrivateChat = new JMenuItem("Start Private Chat");
		popup.add(pmItem);
		popup.add(startPrivateChat);
		
		//listener for popup on userList
		userList.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) {
					popup.show(userList, e.getX(), e.getY());
				}
			}
		});
		
		//listener for popup on lounge userList
		pcUserList.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) {
					popup.show(pcUserList, e.getX(), e.getY());
				}
			}
		});
		
		//listener for private chat initiation
		startPrivateChat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				userList.addSelectionInterval(listModel.indexOf(username), listModel.indexOf(username));
				int[] targets = userList.getSelectedIndices();
				try {
					out.writeObject(new Message(3, "PCREQUEST", username, targets));
					out.flush();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		/*
		 * listener for private messages. automatically selects user that initiates the private chat and changes
		 * their GUI to reflect that they are sending a private message. can also determine if the recipient of 
		 * the private message is in the lounge or not and appends to the respective textarea accordingly.
		 */
		pmItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(inLounge) {
					pcUserList.addSelectionInterval(listModel.indexOf(username), listModel.indexOf(username));
					pmTargets = pcUserList.getSelectedIndices();
					textField.setActionCommand("PM");
					sendBtn.setActionCommand("PM");
					pmLabel.setVisible(true);
					frame.pack();
				}
				else {
					userList.addSelectionInterval(listModel.indexOf(username), listModel.indexOf(username));
					pmTargets = userList.getSelectedIndices();
					textField.setActionCommand("PM");
					sendBtn.setActionCommand("PM");
					pmLabel.setVisible(true);
					frame.pack();
				}
			}
		});
		
		layoutComponent();
		
		/*
		 * textField listener for sending messages. there are three different actioncommands for this textfield
		 * depending on the type of message to be sent. 
		 * REG ---> regular public message
		 * PM ---> private message (after message is sent the GUI returns to the regular mode.)
		 * PC ---> lounge message
		 */
		textField.setActionCommand("REG");
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					if(ae.getActionCommand().equals("REG") && !textField.getText().equals("")) {
						out.writeObject(new Message(1, textField.getText()));
					}
					else if(ae.getActionCommand().equals("PM") && !textField.getText().equals("")) {
						out.writeObject(new Message(5, textField.getText(), username, pmTargets));
						textField.setActionCommand("REG");
						sendBtn.setActionCommand("REG");
						pmLabel.setVisible(false);
					}
					out.flush();
					textField.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		/*
		 * same as above, but for the send button.
		 */
		sendBtn.setActionCommand("REG");
		sendBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					if(ae.getActionCommand().equals("REG") && !textField.getText().equals("")) {
						out.writeObject(new Message(1, textField.getText()));
					}
					else if(ae.getActionCommand().equals("PM") && !textField.getText().equals("")) {
						out.writeObject(new Message(5, textField.getText(), username, pmTargets));
						textField.setActionCommand("REG");
						sendBtn.setActionCommand("REG");
						pmLabel.setVisible(false);
					}
					else if(ae.getActionCommand().equals("PC") && !textField.getText().equals("")){
						out.writeObject(new Message(3, textField.getText()));
					}
					out.flush();
					textField.setText("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		logOffBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				System.exit(0);
			}
		});
		
	}
	
	/**
	 * NOT THE SAME run() as a Thread, just a name.
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	private void run() throws IOException, ClassNotFoundException {
		String serverAddress = getServerAddress();
		Socket socket = new Socket(serverAddress, PORT);
		out = new ObjectOutputStream(socket.getOutputStream());
		in = new ObjectInputStream(socket.getInputStream());
		while(true) {
			Message message = (Message) in.readObject();
			/*
			 * Writes out name as requested by server
			 */
			if(message.getType() == 0 && message.getMessage().equals("SENDMENAME")) {
				out.writeObject(getName());
				out.flush();
			} 
			/*
			 * if name is accepted by server, sets the textField to be editable and displays
			 * the "Logged in as <username>" JLabel
			 */
			else if(message.getType() == 0 && message.getMessage().equals("NAMEACCEPTED")) {
				out.writeObject(new Message(0, "LOGINOCCURRED", username));
				out.flush();
				nameLabel = new JLabel("Logged in as <" + username + ">");
				nameLabel.setVisible(true);
				namePanel.add(nameLabel);
				textField.setEditable(true);
				frame.pack();
			}
			/*
			 * if name is rejected by server, prompts user for a different name
			 */
			else if(message.getType() == 0 && message.getMessage().equals("NAMEREJECTED")) {
				promptDifferentName();
			}
			/*
			 * if regular message, append to public chat textarea.
			 * setCaretPosition is used to always scroll down to the most current message.
			 */
			else if(message.getType() == 1) {
				messageArea.append("<" + message.getUsername() + ">: " + message.getMessage() + "\n");
				messageArea.setCaretPosition(messageArea.getDocument().getLength());
			}
			/*
			 * Login/Logout messages broadcast by server. userList is then updated to reflect the login/logout
			 */
			else if(message.getType() == 2) {
				messageArea.append("<SERVER>: " + message.getMessage() +"\n");
				messageArea.setCaretPosition(messageArea.getDocument().getLength());
				listModel = new DefaultListModel();
				userList.setModel(listModel);
				updateList(userList, message.getUsers());
			} 
			/*
			 * TODO
			 */
			else if(message.getType() == 3 && message.getMessage().equals("PCINVITE")) {
				if(username.equals(message.getUsername())) {
					out.writeObject(new Message(3, "PCACCEPT", username, message.getRoomNum()));
					out.flush();
					pChatRooms.add(new PrivateChat(message.getRoomNum(), out, username));
				}
				else {
					int accept = promptPrivateChat(message.getUsername());
					if(accept == JOptionPane.YES_OPTION) {
						out.writeObject(new Message(3, "PCACCEPT", username, message.getRoomNum()));
						out.flush();
						pChatRooms.add(new PrivateChat(message.getRoomNum(), out, username));
						pChatNums.add((Integer) message.getRoomNum());
					}
				}
			}
			/*
			 * if lounge is accepted, a PCSTARTED message is received from server. lounge is set up on the GUI
			 * and the lounge list is updated to reflect people entering. a message displaying who has entered the
			 * lounge is broadcast to the lounge textArea
			 */
			else if(message.getType() == 3 && message.getMessage().equals("PCSTARTED")) {
				int pChatNum = message.getRoomNum();
				int indexOfDesiredChatNum = 0;
				for(Integer chatNum: pChatNums) {
					if(chatNum == pChatNum)
						indexOfDesiredChatNum = chatNum;
				}
				pChatRooms.get(indexOfDesiredChatNum).appendToPrivateChat("<SERVER>: User <" + message.getUsername() + 
						"> has entered Private Chat #" + message.getRoomNum() + "\n");
			}
			else if(message.getType() == 3 && message.getMessage().equals("PCLEAVE")) {
				int pChatNum = message.getRoomNum();
				int indexOfDesiredChatNum = 0;
				for(Integer chatNum: pChatNums) {
					if(chatNum == pChatNum)
						indexOfDesiredChatNum = chatNum;
				}
				pChatRooms.get(indexOfDesiredChatNum).appendToPrivateChat("<SERVER>: User <" + message.getUsername() + 
						"> has left Private Chat #" + message.getRoomNum() + "\n");
				if(message.getUsername() == username) {
					pChatRooms.remove(indexOfDesiredChatNum);
				}
				
			} 
			/*
			 * if private chat room message, append to that specific chat room's textArea
			 */
			else if(message.getType() == 3 && !message.getMessage().equals("PCINVITE") && !message.getMessage().equals("PCSTARTED")) {
				int pChatNum = message.getRoomNum();
				int indexOfDesiredChatNum = 0;
				for(Integer chatNum: pChatNums) {
					if(chatNum == pChatNum)
						indexOfDesiredChatNum = chatNum;
				}
				pChatRooms.get(indexOfDesiredChatNum).appendToPrivateChat("<" + message.getUsername() + ">: " + message.getMessage() + "\n");
			}
			
			/*
			 * if private message, display with three #'s on either side of the message to denote its private nature.
			 * can determine whether or not the recipient(s) are in the lounge or not and broadcasts accordingly
			 */
			else if(message.getType() == 5) {
				if(pChatRooms.size() > 0) {
					for(PrivateChat chat: pChatRooms) {
						chat.appendToPrivateChat("### <" + message.getUsername() + ">: " + message.getMessage() + " ###\n");
					}
				}
				messageArea.append("### <" + message.getUsername() + ">: " + message.getMessage() + " ###\n");
				messageArea.setCaretPosition(messageArea.getDocument().getLength());
			}
		}
	}
	
	private String getServerAddress() {
		return JOptionPane.showInputDialog(frame, "Enter Server Address", 
				"Server Address", JOptionPane.QUESTION_MESSAGE);
	}
	
	private String getName() {
		username = JOptionPane.showInputDialog(frame, "Pick a Username", 
				"Welcome to Chat Room", JOptionPane.PLAIN_MESSAGE);
		return username;
	}
	
	private void promptDifferentName() {
		JOptionPane.showMessageDialog(frame, 
				"That name is taken. Try a different username.", 
				"Name in Use", JOptionPane.ERROR_MESSAGE);
	}
	
	private int promptPrivateChat(String username) {
		return JOptionPane.showConfirmDialog(frame, "User <" + username + "> has invited you to privately chat. Do you accept?",
				"Private Chat Invite", JOptionPane.YES_NO_OPTION);
	}
	
	/**
	 * Method used to update a particular JList. This enables changes in logins/logouts to be reflected in real-time.
	 * @param userList
	 * @param users
	 */
	private void updateList(JList userList, ArrayList<String> users) {
		DefaultListModel newModel = (DefaultListModel) userList.getModel();
		newModel.clear();
		for(int i = 0; i < users.size(); i++) {
			newModel.addElement((String) users.get(i));
		}
		this.userList.setModel(newModel);
	}
	

	public static void main(String[] args) {
		
				ChatClient client = new ChatClient();
				client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				client.frame.setVisible(true);
				try {
					client.run();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			
		
	}
	
	private void layoutComponent() {
		namePanel = new JPanel();
		nameLabel = new JLabel("nah");
		nameLabel.setVisible(false);
		
		JPanel messagePanel = new JPanel();
		messagePanel.add(pmLabel);
		messagePanel.add(textField);
		messagePanel.add(sendBtn);
		messagePanel.add(logOffBtn);
		pmLabel.setVisible(false);
		
		JPanel nameAndMessagePanel = new JPanel();
		nameAndMessagePanel.setLayout(new BorderLayout());
		nameAndMessagePanel.add(namePanel, BorderLayout.NORTH);
		nameAndMessagePanel.add(messagePanel, BorderLayout.CENTER);
		
		JPanel listPanel = new JPanel();
		userListScroll = new JScrollPane(userList);
		userListScroll.setPreferredSize(new Dimension(125, 150));
		userListScroll.setBorder(BorderFactory.createTitledBorder("Public Chat Users"));
		listPanel.add(userListScroll);
		listPanel.setSize(15, 20);
		
		JPanel outputPanel = new JPanel();
		messageAreaScroll = new JScrollPane(messageArea);
		messageAreaScroll.setBorder(BorderFactory.createTitledBorder("Public Chat"));
		outputPanel.add(messageAreaScroll);
		
		frame.setLayout(new BorderLayout());
		frame.add(nameAndMessagePanel, BorderLayout.NORTH);
		frame.add(listPanel, BorderLayout.WEST);
		frame.add(outputPanel, BorderLayout.CENTER);
		frame.pack();
		
	}

}
