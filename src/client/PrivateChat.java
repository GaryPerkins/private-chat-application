package client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import data.Message;

public class PrivateChat {
	private ObjectOutputStream out;
	JFrame frame;
	JTextField textField = new JTextField(40);
	JTextArea messageArea = new JTextArea(8, 40);
	JButton sendBtn = new JButton("Send");
	JButton leavePCChatBtn = new JButton("Leave Private Chat");
	private String username;
	private JScrollPane messageAreaScroll;
	private int roomNum;
	
	public PrivateChat(final int roomNum, final ObjectOutputStream out, final String username) {
		//set up port to communicate with server
		this.roomNum = roomNum;
		this.out = out;
		this.username = username;
		
		frame = new JFrame("Private Chat Room #" + roomNum);
		
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(!textField.getText().equals("")) {
					try {
						out.writeObject(new Message(3, textField.getText(), roomNum));
						out.flush();
						textField.setText("");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		sendBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(!textField.getText().equals("")) {
					try {
						out.writeObject(new Message(3, textField.getText(), roomNum));
						out.flush();
						textField.setText("");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		leavePCChatBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					out.writeObject(new Message(3, "PCLEAVE", username, roomNum));
					out.flush();
					frame.setVisible(false);
				} catch (IOException e) { 
					e.printStackTrace();
				}
			}
		});
		layoutComponent();
	}
	
	private void layoutComponent() {
		
		JPanel messagePanel = new JPanel();
		messagePanel.add(textField);
		messagePanel.add(sendBtn);
		messagePanel.add(leavePCChatBtn);
		
		JPanel nameAndMessagePanel = new JPanel();
		nameAndMessagePanel.setLayout(new BorderLayout());
		nameAndMessagePanel.add(messagePanel, BorderLayout.CENTER);
		
		JPanel outputPanel = new JPanel();
		messageArea.setEditable(false);
		messageAreaScroll = new JScrollPane(messageArea);
		messageAreaScroll.setBorder(BorderFactory.createTitledBorder("Private Chat"));
		outputPanel.add(messageAreaScroll);
		
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(nameAndMessagePanel, BorderLayout.NORTH);
		frame.add(outputPanel, BorderLayout.CENTER);
		frame.setVisible(true);
		frame.pack();
		
	}
	
	void appendToPrivateChat(String msg) {
		messageArea.append(msg);
	}
	
	public int getRoomNum() {
		return roomNum;
	}

}
