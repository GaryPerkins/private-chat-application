package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import data.Message;

public class ChatServer {
	
	private static final int PORT = 9000;
	private static ArrayList<String> names = new ArrayList<String>();
	private static ArrayList<ObjectOutputStream> writers = new ArrayList<ObjectOutputStream>();
	private static ArrayList<ArrayList<String>> privateRoomNames = new ArrayList<ArrayList<String>>();
	private static ArrayList<ArrayList<ObjectOutputStream>> privateRoomWriters = new ArrayList<ArrayList<ObjectOutputStream>>();
	private static int roomNum = 0;
	
	
	public static void main(String[] args) throws IOException {
		System.out.println("The chat server is running.");
		@SuppressWarnings("resource")
		ServerSocket listener = new ServerSocket(PORT);
		while(true) {
			new Handler(listener.accept()).start();
		}
	}
	
	private static class Handler extends Thread {
		private String name;
		private Socket socket;
		private ObjectInputStream in;
		private ObjectOutputStream out;
		
		public Handler(Socket socket) {
			this.socket = socket;
		}
		
		public void run() {
			try {
				out = new ObjectOutputStream(socket.getOutputStream());
				in = new ObjectInputStream(socket.getInputStream());
				while(true) {
					out.writeObject(new Message(0, "SENDMENAME"));
					out.flush();
					name = (String) in.readObject();
					if(name == null) {
						return;
					}
					
					synchronized(names) {
						if(!names.contains(name)) {
							names.add(name);
							break;
						}
						else {
							out.writeObject(new Message(0, "NAMEREJECTED"));
							out.flush();
						}
					}
				}
				
				out.writeObject(new Message(0, "NAMEACCEPTED"));
				out.flush();
				synchronized(writers) {
					writers.add(out);
				}
				
				// wait for incoming messages...
				while(true) {
					Message input = (Message) in.readObject();
					if(input == null) {
						return;
					}
					/*
					 * RESETS the outputstream in order to reflect any changes made in the ArrayLists since the last time
					 * they were sent out. This particular conditional broadcasts logins to all outputstreams in the
					 * writers ArrayList.
					 */
					else if(input.getType() == 0 && input.getMessage().equals("LOGINOCCURRED")) {
						for(ObjectOutputStream writer: writers) {
							writer.reset();
							writer.writeObject(new Message(2, "User <" + 
									input.getUsername() + "> has logged in.", names));
							writer.flush();
						}
					}
					else if(input.getType() == 3 && input.getMessage().equals("PCREQUEST")) {
						for(int index: input.getPCTargets()) {
							ObjectOutputStream targetWriter = writers.get(index);
							targetWriter.writeObject(new Message(3, "PCINVITE", input.getUsername(), roomNum));
							targetWriter.flush();
						}
						privateRoomNames.add(new ArrayList<String>());
						privateRoomWriters.add(new ArrayList<ObjectOutputStream>());
						roomNum++;
					}
					else if(input.getType() == 3 && input.getMessage().equals("PCACCEPT")) {
						int index = names.indexOf(input.getUsername());
						synchronized(privateRoomNames) {
							privateRoomNames.get(input.getRoomNum()).add(input.getUsername());
						}
						synchronized(privateRoomWriters) {
							privateRoomWriters.get(input.getRoomNum()).add(writers.get(index));
						}
						//TODO: LIST IMPLEMENTATION
						for(ObjectOutputStream writer: privateRoomWriters.get(input.getRoomNum())) {
							writer.reset();
							writer.writeObject(new Message(3, "PCSTARTED", input.getUsername(), privateRoomNames.get(input.getRoomNum()), input.getRoomNum()));
							writer.flush();
						}
					}
					
					/*
					 * Removes the user requesting to leave the private chat's name and associated writer from the specific room number's arraylist.
					 * If that room is empty after removal, dereference the entire room number's arraylist and let it be garbage collected.
					 * Otherwise, broadcast the fact that the user left to those left in the channel.
					 */
					else if(input.getType() == 3 && input.getMessage().equals("PCLEAVE")) {
						int indexOfName = privateRoomNames.get(input.getRoomNum()).indexOf(input.getUsername());
						Integer roomNum = new Integer(input.getRoomNum());
						String username = String.valueOf(input.getUsername());
						
						synchronized(privateRoomNames) {
							privateRoomNames.get(roomNum).remove(indexOfName);
						}
						synchronized(privateRoomWriters) {
							privateRoomWriters.get(roomNum).remove(indexOfName);
						}
						if(privateRoomNames.get(input.getRoomNum()).size() == 0) {
							synchronized(privateRoomNames) {
								privateRoomNames.remove(input.getRoomNum());
							}
							synchronized(privateRoomWriters) {
								privateRoomWriters.remove(input.getRoomNum());
							}
						}
						else {
							for(ObjectOutputStream writer: privateRoomWriters.get(input.getRoomNum())) {
								writer.reset();
								writer.writeObject(new Message(3, "PCLEAVE", username, privateRoomNames.get(input.getRoomNum()), input.getRoomNum()));
								writer.flush();
							}
						}
					}
					
					else if(input.getType() == 3 && !input.getMessage().equals("PCREQUEST") && !input.getMessage().equals("PCACCEPT") && !input.getMessage().equals("LEAVEREQUEST")) {
						for(ObjectOutputStream writer: privateRoomWriters.get(input.getRoomNum())) {
							writer.writeObject(new Message(3, input.getMessage(), name, input.getRoomNum()));
							writer.flush();
						}
					}
					/*
					 * If the message is type 5, this indicates a private message. Because of the matching indices of the
					 * names and writers ArrayLists, the private message targets are sent over from the client and the 
					 * server is able to write out these private message objects to the appropriate outputstreams.
					 */
					else if(input.getType() == 5) {
						for(int writer: input.getPCTargets()) {
							writers.get(writer).writeObject(new Message(5, input.getMessage(), input.getUsername()));
							writers.get(writer).flush();
						}
					}
					/*
					 * If nothing above applies to the message sent over by client, the server will treat the message
					 * as a regular message and send it out to all writers currently in the ArrayList.
					 */
					else {
						for(ObjectOutputStream writer: writers) {
							writer.writeObject(new Message(1, input.getMessage(), name));
							writer.flush();
						}
					}
				}
			} catch (IOException e) {
				// do nothing
			} catch (ClassNotFoundException e) {
				// do nothing
			} 
			/*
			 * finally statement for handling a user suddenly terminating a session. this statement will
			 * remove the terminated user from the writers and names ArrayLists if their values are non-null.
			 * After this, the server will broadcast to those still in the ArrayLists that the terminated client
			 * has logged out of the chat server. At the end of all of this, the server closes the socket of the
			 * terminated client.
			 */
			
			finally {
				if(out != null) {
					writers.remove(out);
				}
				if(name != null) {
					names.remove(name);
					for(ObjectOutputStream writer: writers) {
						try {
							writer.reset();
							writer.writeObject(new Message(2, "User <" + name + "> has logged out.", names));
							writer.flush();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							
						}
					}
				}
				try {
					socket.close();
				} catch (IOException e) {
					//do nothing
				}
			}
		}
	}
}
